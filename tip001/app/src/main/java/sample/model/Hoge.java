package sample.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
// @JsonPropertyOrder({"name", "id"})
public class Hoge {

  // @JsonProperty("id")
  private String id;

  // @JsonProperty("name")
  private String name;

}
