package sample.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

// @Getter
// @Setter
// @ToString
// @NoArgsConstructor
// @AllArgsConstructor
// @JsonPropertyOrder({"my name", "my id"})
// @JsonIgnoreProperties(ignoreUnknown = true)
public class HogeCsv {

  @JsonProperty("my id")
  private String id;

  @JsonIgnore
  // @JsonProperty("my name")
  private String name;

}
